"use strict";

// const developers = [
//     {
//         name: 'stephen',
//         email: 'stephen@appddictionstudio.com',
//         languages: ['html', 'css', 'javascript', 'php', 'java']
//     },
//     {
//         name: 'karen',
//         email: 'karen@appddictionstudio.com',
//         languages: ['java', 'javascript', 'angular', 'spring boot']
//     },
//     {
//         name: 'juan',
//         email: 'juan@appddictionstudio.com',
//         languages: ['java', 'aws', 'php']
//     },
//     {
//         name: 'faith',
//         email: 'faith@codebound.com',
//         languages: ['javascript', 'java', 'sql']
//     },
//     {
//         name: 'dwight',
//         email: 'dwight@codebound.com',
//         languages: ['html', 'angular', 'javascript', 'sql']
//     }
//
// ];

// // \// TODO: rewrite the object literal using object property shorthand
// // developers.push({
// //     name: name,
// //     email: email,
// //     languages:
// // });
// // console.log(developers);
//
//
// const name = 'eric';
// const email = 'victorericchavez@gmail.com';
// const languages = '\'html\', \'jquery\', \'javascript\', \'css\'';
// developers.push({
//     name,
//     email,
//     languages,
// });
// console.log(developers);
//
//
// // // TODO: replace `var` with `let` in the following variable declarations
// // var emails = [];
// // var names = [];
//
//
// let emails = [];
// let names = [];
//
//
//
//
//
//
// // // TODO: rewrite the following using arrow functions
// // developers.forEach(function(developer) {
// //     return emails.push(developer.email);
// // });
// // console.log(emails);
// // developers.forEach(function(developer) {
// //     return names.push(developer.name);
// // });
// // console.log(names);
// // developers.forEach(function (developer) {
// //     return languages.push(developer.languages);
// // });
// developers.forEach(developer => emails.push(developer.email));
//
// developers.forEach(developer => names.push(developer.name));
//
// developers.forEach(developer => languages.push(developer.languages));
//
//
//
//
//
// //
// // // TODO: replace `var` with `const` in the following declaration
//
// const developerTeam = [];
// developers.forEach(function(developer) {
//
//
// //     // TODO: rewrite the code below to use object destructuring assignment
// //     //       note that you can also use destructuring assignment in the function
// //     //       parameter definition
//
//     // const name = developer.name;
//     // const email = developer.email;
//     // const languages = developer.languages;
//
//
//     const  {name, email, languages} = developer;
//
// // // TODO: rewrite the assignment below to use template strings
// //
// //     developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
// //
// // });
// // console.log(developerTeam);
//
//     developerTeam.push(`${name}'s + email is  ${email}  ${name}  knows  ${languages.join(', ')} `);
// //
//
// //
// // // TODO: Use `const` for the following variable
//        const list = '<ul>';
// // TODO: rewrite the following loop to use a for..of loop
// // developers.forEach(function (developer) {
// //
// //
// //     // TODO: rewrite the assignment below to use template strings
// //     list += '<li>' + developer + '</li>';
// //
// // });
//
// // list += '</ul>';
//
//     document.write(list);
//
// // FOR... OF LOOP
//     for (const developer of developers ) {
//         // TODO: rewrite the assignment below to use template strings
//         // list += '<li>' + developer + '</li>';
//
//         list += `<li>${developer}</li>`
//
//     }
//     list += '</ul>';
//
// // for of loop SYNTAX:
// // for (const element of iterable) {
// //
// // }
// const petName = 'Chula';
// const age = 14;
// // REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + petName + ' and she is ' + age + ' years old.');
//
// console.log(`My dog is named${petName} and she is ${age} years old.`);
//
//
// // //REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// // function addTen(num1) {
// //     return num1 + 10;
// // }
// const addTen =(num1) => num1 + 10;
//
//
// // // REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// // function minusFive(num1) {
// //     return num1 - 5;
// // }
//
// const minusFive =(num1) => num1 - 5;
//
//
// // REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// // function multiplyByTwo(num1) {
// //     return num1 * 2;
// // }
// const multiplyByTwo =(num1) => num1 * 2;
//
//
// // REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };
//
//
// const greeting = name => console.log( "Hello, " + name + ' how are you?');
// }
//
// // REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }
// function(const haveWeMet) => name + "Nice to see you again";{
//
// }else {
//     =>"Nice to meet you"
//
// }
//
// const haveWeMet = name => (name === 'Bob')? 'Nice to see you again'
// :('Nice to meet you');
//





// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
//
// var odds = [];
//
// for (var i = 0 ; i < numbers.length; i++) {
// if (numbers[i] % 2 === 1) {
//     odds.push(numbers[i]);
//     }
//  }

// console.log(odds);




// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// var odds = numbers.filter(function (n) {
//     return n % 2 !==0;
// });

// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
//
// const even = numbers.filter(n => n % 2 ===0);
//
// const odds = numbers.filter(n => n % 2 ===0);
//
//
const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'faith',
        email: 'faith@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];
// /**Use .filter to create an array of developer objects where they have
//  at least 5 languages in the languages array*/

// const moreLang =developers.filter(developer => developer.languages.length >= 5);
//
//
// console.log(moreLang);

// /**Use .map to create an array of strings where each element is a developer's
//  email address*/

//
// const emailAddress = developers.map(developer => developer.email);
//
//
// console.log(emailAddress);

// /**Use reduce to get the total years of experience from the list of developers.
//  - Once you get the total of years you can use the result to calculate the average.*/

//
// const totalYears = developers.reduce((total,years) =>{
//     return total + years.yearExperience;
// },0);
// // console.log(totalYears)
//
// const average = totalYears/developers.length;
// console.log(average)
//


// /**Use reduce to get the longest email from the list.*/

//
// const longEmail = developers.reduce((longest,nW) => {
//     return longest email.length > nW.email.length ? a : b;
// },email);
//
// console.log(longEmail);


// /**Use reduce to get the list of developer's names in a single string
//  - output:
//  CodeBound Staff: stephen, karen, juan, faith, dwight*/
//
// const devNames = developers.reduce((a,b) => {
//     return a + b.name + ', ';
// },'Codebound Staff:');
//
// console.log(devNames)
//


// /** BONUS: use reduce to get the unique list of languages from the list
//  of developers
//  */