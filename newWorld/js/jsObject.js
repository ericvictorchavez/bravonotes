"use strict";


//* Create an object with firstName and lastName properties that are strings
// *  with your first and last name. Store this object in a variable named
// * `person`.
// *
// * Example:
// *  > console.log(person.firstName) // "Tim"
// *  > console.log(person.lastName) // "Duncan"
// *

var person = {

    firstName: 'eric',

    lastName: 'chavez'
}

console.log(person.firstName);
console.log(person.lastName);

// * Add a sayHello method to the person object that returns a greeting using
// * the firstName and lastName properties.
// * console.log the returned message to check your work
// *
// * Example
// * > console.log(person.sayHello()) // "Hello from Tim Duncan!"

var person = {};

person.firstName= 'eric'
person.lastName= 'chavez'
person.describePerson = function () {
    console.log("Hello from " + this.firstName + ' ' + this.lastName)
}
person.describePerson();





// * HEB has an offer for the shoppers that buy products amounting to
// * more than $200. If a shopper spends more than $200, they get a 12%
// * discount. Write a JS program, using conditionals, that logs to the
// * browser, how much Karen, Pibo and Juan need to pay. We know that
// * Pibo bought $180, Karen $250 and Juan $320. Your program will have to
// * display a line with the name of the person, the amount before the
// * discount, the discount, if any, and the amount after the discount.
// *
// * Use a foreach loop to iterate through the array,
// * and console.log the relevant messages for each person
// *
// * Use the following array of objects


var shoppers =[

    {name: 'Pibo', amount: 180},
    {name: 'Karen', amount: 250},
    {name: 'Juan', amount: 320}
];

var discount = .12;

shoppers.forEach(function (shopper) {
    var output = `${shopper.name} bought $${shopper.amount}.`;

    if (shopper.amount >= 200) {

        output += '\nDiscount: ' + discount + '\nTotal: $ ' + (shopper.amount * (1 - discount));

    }

    console.log(output);
})







// * Create an array of objects that represent books and store it in a
// * variable named `books`. Each object should have a title and an author
// * property. The author property should be an object with properties
// * `firstName` and `lastName`. Be creative and add at least 5 books to the
// * array
// */
// /**
//  * Example:
//  * > console.log(books[0].title) // "IT"
//  * > console.log(books[0].author.firstName) // "Stephen"
//  * > console.log(books[0].a

var book =[
    {
        title: 'IT',
        author:{
            firstName: 'Stephen ',
            lastName: 'King',
        }


    },
    {
        title: 'The Handmaid\'s Tale',
        author:{
            firstName: 'Margaret ',
            lastName: 'Atwood',
        }


    },
    {
        title: 'Fire and Bloddd',
        author:{
            firstName: 'George ',
            lastName: 'R.R. Martin',
        }


    },
    {
        title: 'Harry Potter and the Prisoner of Azkaban',
        author:{
            firstName: 'J.K. ',
            lastName: 'Rowling',
        }


    },
    {
        title: 'Batman NightWalker',
        author:{
            firstName: 'S ',
            lastName: 'King',
        }


    }
];


//  * Loop through the books array and output the following information about
//  * each book:
//  * - the book number (use the index of the book in the array)
//  * - the book title
//  * - author's full name (first name + last name)
//  */
// /**
//  * Example Console Output:
//  *
//  *      Book # 1
//  *      Title: IT
//  *      Author: Stephen King
//  *      ---
//  *      Book # 2
//  *      Title: The Handmaid's Tale
//  *      Author: Margaret Atwood
//  *      ---
//  *      Book # 3
//  *      Title: Fire and Blood
//  *      Author: George R.R. Martin
//  *      ---
//  *      ...
//  */

console.log(book[0].title)
console.log(book[0].author.firstName)
console.log(book[0].author.lastName)

for (var i = 0; i < book.length; i++){
    console.log(
        'Book # ' + parseInt(i + 1) +
        '\nTitle: ' + book[i].title +
        '\nAuthor: ' + book[i].author.firstName + ' ' + book[i].author.lastName +
        '\n--'

    );

}





// 1. create a for loop function that logs every 'Pop' album
// 2. create a for each function that logs every 'Rock' album
// 3. create a for each function that logs every album released before 2000
// 4. create a for loop function that logs every album between 1990 - 2020
// 5. for loop function that logs every Michael Jackson album
// 6. create a function name 'addAlbum' that accepts the same parameters from
// 'albums' and add it to the array
const albums = [
    {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}

]
// 1. create a for loop function that logs every 'Pop' album


for (var i = 0; i < albums.length; i++) {

    if (albums[i].genre === 'Pop') {

        console.log(albums[i].title)

    }
}

// 2. create a for each function that logs every 'Rock' album

albums.forEach(function (album, x) {

    if (albums[x].genre === 'Rock') {

        console.log(albums[x].title)
    }
})



for (var i = 0; i < albums.length; i++) {

    if (albums[i].genre === 'Rock') {

        console.log(albums[i].artist)
    }
}

// 3. create a for each function that logs every album released before 2000
//
// albums.forEach(function (album, i) {
//
//     if (albums[i].released() < 2000) {
//
//         console.log(albums[i].released >= 1990 );
//     }
// })


// 4. create a for loop function that logs every album between 1990 - 2020


// for (var i = 0; i < albums.length; i++) {
//
//     if (albums[i].released >= 1990 && albums[i].released <
//         2020){
//
//         console.log(albums[i].artist + '  ' +  )
//     }
//
// }



// 5. for loop function that logs every Michael Jackson album

for (var i = 0; i < albums.length; i++){
    if(albums[i].artist === "Michael Jackson"){

        console.log('Here it is ' + albums[i].title)
    }
}

// 6. create a function name 'addAlbum' that accepts the same parameters from
// 'albums' and add it to the array

function addAlbum(artist, title, released, genre){

    albums.push(
        {
            'artist':artist,
            'title': title,
            'released': released,
            'genre': genre,

        }
    )
}
addAlbum('Judas Priest','Hell Bent',1982,'Rock');
console.log(albums);



