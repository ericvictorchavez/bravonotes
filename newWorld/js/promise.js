"use strict"
// fetch('http://localhost')
//     .then(response => console.log(response))
//     .catch(error => console.log(error));
// const myPromise = fetch('http://');
// myPromise.then(response => console.log(response));
// myPromise.catch(error => console.log(error));
// Promise.all accepts an array of promises, and resolves when all the individual promises have resolves
// Promise.race: accepts an array of promises, and resolves when the first promise resolves
//Chaining Promises together - the return value from a promises callback can itself be treated as a promise,
// which allows us to chain promises together. Lets look at an example
// Promise.resolve to immediately return a resolved promise


/** We can create a promise object like so*/
// const myPromise = new Promise((resolve, reject) => {
//     if(Math.random()>0.5){
//         resolve();
//     } else{
//         reject();
//     }
// });
// myPromise.then(() => console.log('resolved'));
// myPromise.catch(() =>  console.log('rejected'));


/** Discovering pending*/
//
// const myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if (Math.random() > 0.5) {
//             resolve();
//         } else {
//             reject();
//         }
//
//     },1500);
// });
// myPromise.then(() => console.log('resolved'));
// myPromise.catch(() =>  console.log('rejected'));


/**Write aprimes that simulates and api request*/




const data = {username: 'example'};

fetch('surprise.html', {
    method: 'POST',
    headers: {
        'Content-Type' : 'application/json',
    },
    body: JSON.stringify(data),
})
    .then(response => response.json())
    .then(data =>{
        console.log('success', data)
    })
    .catch((error) => {
        console.log('Error fetch gone bad', error);
    })


const isMomHappy = true;

//Promise
const willGetNewPhone = new Promise (((resolve, reject) =>
    {if(isMomHappy) {
    const phone = {
        brand: 'Samsung',
        color: 'black'
    };
    resolve(phone);
}else{
        const reason = new Error('mom is not happy')
    reject(reason);


    }}))

//2nd promise
const showoff =function(phone){
    const message = "hey friend I have a new" +
        phone.brand + '' + phone.color + 'phone';
    return Promise.resolve(message);
}

//call our promise
const askMom = function () {
    willGetNewPhone
        .then(showoff)
        .then(fulfilled => console.log(fulfilled)
        )
        .catch(error => console.log(error.message))}

askMom();



















