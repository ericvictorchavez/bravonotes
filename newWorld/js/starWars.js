"use strict";
//
// $(document).ready(function(){
//     $.ajax({
//         url: 'data/starWars.json',
//         type: 'GET',
//         dataType: 'JSON',
//         success: function(response){
//             var len = response.length;
//             for(var i=0; i<len; i++){
//                 var name = response[i].name;
//                 var height = response[i].height;
//                 var mass = response[i].mass;
//                 var hair_color = response[i].hair_color;
//                 var skin_color = response[i].skin_color;
//                 var eye_color = response[i].eye_color;
//                 var birth_year = response[i].birth_year;
//                 var gender = response[i].gender;
//
//                 var tr_str = "<tr>" +
//                     "<td align='center'>" + (i+1) + "</td>" +
//                     "<td align='center'>" + name + "</td>" +
//                     "<td align='center'>" + height + "</td>" +
//                     "<td align='center'>" + mass + "</td>" +
//                     "<td align='center'>" + hair_color + "</td>" +
//                     "<td align='center'>" + skin_color + "</td>" +
//                     "<td align='center'>" + eye_color + "</td>" +
//                     "<td align='center'>" + birth_year + "</td>" +
//                     "<td align='center'>" + gender + "</td>" +
//
//                     "</tr>";
//
//                 $("#character tbody").append(tr_str);
//             }
//
//         }
//     });
// });
// myRequest.send();

 $(document).ready(function(){
    $.ajax({
        url: '../data/starWars.json',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var name = response[i].name;
                var height = response[i].height;
                var mass = response[i].mass;
                var birthYear = response[i].birth_year;
                var gender = response[i].gender;
                var tr_str = "<tr>" +
                    "<td align='center'>" + name + "</td>" +
                    "<td align='center'>" + height + "</td>" +
                    "<td align='center'>" + mass + "</td>" +
                    "<td align='center'>" + birthYear + "</td>" +
                    "<td align='center'>" + gender + "</td>" +
                    "</tr>";
                $("#insertCharacters").append(tr_str);
            }
        }
    });
 });
// $.ajax('data/starwars.json',{
//     type: 'Get',
// }).done(function(data){
//     $.each(data,function(index, value){
//         console.log(value);
//         var name = value.name;
//         var height = value.height;
//         var mass = value.mass;
//         var birth = value.birth_year;
//         var gender = value.gender;
//         var char =  '<tr>' + '<td>' + name + '</td>'
//             + '<td>' + height + '</td>'
//             + '<td>' + mass + '</td>' +
//             '<td>' + birth + '</td>' +
//             '<td>' + gender + '</td>' + '</tr>';
//         $('#insertCharacters').append(char);
//     })
// })


