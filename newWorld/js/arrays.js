(function  () {
"use strict"

    //['basketball,baseball,soccer,football,hockey']

    // var sports=[];
    //
    // var sports2 =['basketball','baseball','soccer','football','hockey']
    //
    // sports2.sort();
    // console.log(sports2);
    //
    //
    // var sports3 =['basketball','baseball','soccer','football','hockey']
    //
    // sports3.reverse()
    // console.log(sports3)
    //
    //
    // var sports4 =['basketball','baseball','soccer','football','hockey','volleyball','tennis','golf']
    //
    //
    //  var index = sports4.lastIndexOf('soccer');
    //
    // console.log(index);
    //
    // var sports5 =['basketball','baseball','soccer','football','hockey','volleyball','tennis','golf']
    //
    // var removedfootball = sports5.shift();
    // console.log("football: " + removedfootball);
    //
    // sports.shift();
    // console.log(sports)
    //
    // var sports =['basketball,baseball,soccer,football,hockey,volleyball,tennis,golf']
    //


    //** Create a new array named bravos that holds the names of the people in your class**/

    var bravos= ['adrian','henry','jonathan','maryann','sandra','tameka','eric']


    //** Log out each individual name from the array by accessing the index. **/

    console.log(bravos.indexOf('adrian'))

    var first = bravos[1]
    console.log(first)


    //** Using a for loop, Log out every item in the bravos array **/

    for (var i = 0; i < bravos.length; i += 1) {
        console.log('the person at index' +' '+ i + '' + ' is ' +'' + bravos[i]);
    }

    //** Refactor your code using a forEach Function **/

    bravos.forEach(function (bravos) {
        console.log(bravos)
         })

    //** Using a for loop. Remove all instance of 'CodeBound' in the array **/

    var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];

    for(var n = 0; n < arrays1.length; n++){

        if(arrays1[n] !== 'CodeBound'){

            console.log(arrays1[n]);
        }
    }

    //**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/

    //var numbers = [3, '12', 55, 9, '0', 99];

    var numbers = [3, '12', 55, 9, '0', 99];

    var sum = 0

    numbers.forEach(function (num) {

        sum += parseInt(num);
    });

    console.log(sum);

    //**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/
    // var numbers1 = [7, 10, 22, 87, 30];

    var numbers = [7, 10, 22, 87, 30];

    numbers.forEach(function (element,index,numbers) {

        numbers[index] = element * 5;
    })
    console.log(numbers)














})()