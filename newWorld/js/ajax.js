// "use strict"
//
// var i =new XMLHttpRequest();
// i.open(
//     'GET' ,'surprise.html');
// i.onreadystatechange = function () {
//
//
//     if (i.readyState === 4) {
//         document.getElementById('ajax-content').innerHTML = i.responseText;
//     }
// };
//
//     function sendTheAJAX() {
//         i.send();
//         document.getElementById('reveal').style.display = 'none';
//
//     }
"use strict";
// 1. create a new XMLHttpRequest object -- an object like any other!
var myRequest = new XMLHttpRequest();
// 2. open the request and pass the HTTP method name and the resource as parameters
myRequest.open('GET', 'surprise.html');
// 3. write a function that runs anytime the state of the AJAX request changes
myRequest.onreadystatechange = function () {
    // 4. check if the request has a readyState of 4, which indicates the server has responded (complete)
    if (myRequest.readyState === 4) {
        // 5. insert the text sent by the server into the HTML of the 'ajax-content'
        document.getElementById('ajax-content').innerHTML = myRequest.responseText;
    }
};
function sendTheAJAX() {
    myRequest.send();
    document.getElementById('reveal').style.display = 'none';
}